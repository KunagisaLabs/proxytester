# ProxyTester

A simple C++ program to test proxy with libcurl (With multiple threads)

## How to build

First, You need to have curl(libcurl) presents in Your system.  
Then 
~~~~
cmake . -B cmake-build
cd cmake-build && make
~~~~
And you'll get proxytester

## Usage:
~~~~
proxychecker sourcefile passedoutput 
~~~~

Please notice that the source file needs to be the following format:  

proto://ip:port

Example:
~~~~
socks5://127.0.0.1:1080
http://114.5.1.4:8080
~~~~