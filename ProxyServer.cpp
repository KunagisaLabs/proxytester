#include "ProxyServer.h"
#include <string>
#include <curl/curl.h>

std::string ProxyServer::extract_proto(const std::string &proxy) {
    std::string accepted_scheme_string[4] = { "socks4","socks5","http","https" };// Allowed Scheme Types
    std::string str_buff;
    int splitter_buff;
    bool proto_validate_flag = false;
    splitter_buff = proxy.find_first_of(':');

    if (std::string::npos != splitter_buff) {
        str_buff = proxy.substr(0, splitter_buff);
        for (int i = 0; i < accepted_scheme_string->size(); i++)
        {
            if (str_buff == accepted_scheme_string[i]) {
                proto_validate_flag = true;
            }
        }

        if (proto_validate_flag){
            return str_buff;
        }
        else{
            throw ProxyException("Scheme Error");
        }
    }
    else{
        throw ProxyException("Scheme Error");
    }
}

std::string ProxyServer::extract_address(const std::string& proxy){
    std::string str_buff;
    int splitter_buff;
    int ipseg[4];
    // Border check
    splitter_buff=proxy.find_last_of('/');
    if(std::string::npos==splitter_buff){throw ProxyException("Address Error");}
    if(std::string::npos==proxy.find_last_of(':')){throw ProxyException("Address Error");}
    if(proxy.find_last_of(':')==proxy.find_first_of(':')){throw ProxyException("Address Error");}

    try {str_buff = proxy.substr(splitter_buff+1,
                                 (proxy.find_last_of(':')-splitter_buff-1));}
    catch (const std::out_of_range &) {throw ProxyException("Address Error");}

    splitter_buff=0; // Reset splitter_buff
    try {
        for(int & i : ipseg){
            i=stoi(str_buff.substr(splitter_buff,str_buff.find_first_of('.',splitter_buff)));
            splitter_buff=str_buff.find_first_of('.',splitter_buff)+1;
        }
    }
    catch (const std::invalid_argument &){throw ProxyException("Address Error");}
    for(int i : ipseg){
        if(i>255||i<0){throw ProxyException("Address Error");}
    }
    return str_buff;
}

std::string ProxyServer::extract_port(const std::string& proxy)
{
    std::string str_buff;
    int splitter_buff;
    int port_buff;
    splitter_buff=proxy.find_last_of(':');
    // Border check
    if(std::string::npos==splitter_buff){throw ProxyException("Port Error");}
    if(splitter_buff==proxy.find_first_of(':')){throw ProxyException("Port Error");}
    try{
        port_buff=stoi(proxy.substr(splitter_buff+1));
    }
    catch (const std::out_of_range &){throw ProxyException("Port Error");}
    if(port_buff<0||port_buff>65535){throw ProxyException("Port Error");}
    else{return proxy.substr(splitter_buff+1);}
}

int ProxyServer::proto()
{
    if("socks4" == proxy_scheme_){return CURLPROXY_SOCKS4;}
    else if ("socks5" == proxy_scheme_){return CURLPROXY_SOCKS5;}
    else if ("http" == proxy_scheme_){return CURLPROXY_HTTP;}
    else if ("https" == proxy_scheme_){return CURLPROXY_HTTPS;}
    else {return -1;}
}


bool ProxyServer::proxy_check()
{
    CURL * curl;
    CURLcode res;
    long resp_code;
    curl = curl_easy_init();
    if(curl){
        curl_easy_setopt(curl,CURLOPT_URL, "http://www.qualcomm.com/generate_204");
        curl_easy_setopt(curl,CURLOPT_PROXY,proxy_address_.c_str());
        curl_easy_setopt(curl,CURLOPT_PROXYPORT,this->port());
        curl_easy_setopt(curl,CURLOPT_PROXYTYPE,this->proto());
        curl_easy_setopt(curl,CURLOPT_TIMEOUT,2);
        res = curl_easy_perform(curl);
        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &resp_code);

        if(res != CURLE_OK){
            curl_easy_cleanup(curl);
            return false;
        }
        else{
            return resp_code == 301;
        }
    }
    else {return false;}
}

ProxyException::ProxyException(const std::string& cause) {
    if(cause == "Scheme Error"){type_=PROXYERROR_SCHEME_ERROR;}
    else if(cause =="Address Error"){type_=PROXYERROR_ADDRESS_ERROR;}
    else if(cause == "Port Error"){type_=PROXYERROR_PORT_ERROR;}
    std::cerr << "ProxyError: ";
    std::cerr << cause.c_str();
}
