#pragma once
#include <string>
#include <cstdbool>
#include <iostream>
#include <curl/curl.h>

// ProxyError Exceptions
#define PROXYERROR_SCHEME_ERROR 1
#define PROXYERROR_ADDRESS_ERROR 2
#define PROXYERROR_PORT_ERROR 3
class ProxyException : std::exception
{
private:
    int type_=0;
public:
    explicit ProxyException(const std::string& cause);
    int ExecptionType(){return type_;}
    ~ProxyException() override = default;
};// An Exception to Error while checking proxy

class ProxyServer
{
private:
    // Proxy Server info, Use the following patterns:
    // scheme://address:port
    std::string proxy_scheme_;// Scheme
    std::string proxy_address_;
    std::string proxy_port_;
    std::string extract_proto(const std::string & proxy);//Extract Proctol Scheme from Server string.
    static std::string extract_address(const std::string & proxy);
    static std::string extract_port(const std::string & proxy);
public:
    explicit ProxyServer(const std::string & proxy_str) {
        // Check scheme,if fail to match Throw ProxyException.ExceptionType=1
        proxy_scheme_=extract_proto(proxy_str);
        // Validate Server IP
        proxy_address_=extract_address(proxy_str);
        //Validate Port
        proxy_port_=extract_port(proxy_str);
    } // Creator using proxy string
    bool proxy_check();
    ~ProxyServer()= default;// Void dtor
    int port(){return stoi(proxy_port_);}
    int proto();//
    std::string address(){return proxy_address_;}
};

