//
// Created by kunagisa on 7/21/19.
//

#include <iostream>
#include <fstream>
#include "ProxyServer.h"
#include <curl/curl.h>
#include <future>
#include <vector>
using std::vector;
using std::string;
using std::mutex;
mutex locker;
string proxychecker(vector<string> * list,string proxy){
    ProxyServer p(proxy);
    if(p.proxy_check()){
        locker.lock();
        list->push_back(proxy);
        locker.unlock();
    }
    return "";
}

int main(int argc,char** argv){
    curl_global_init(CURL_GLOBAL_ALL);
    if(1 == argc){
        std::cout << string(argv[0])+"needs file to check" << std::endl;
        std::cout << "Usage: "+string(argv[0])+" file_to_check file_to_output" << std::endl;
        std::cout << "Files should using the following pattern" << std::endl;
        std::cout << "proto://ip:port" << std::endl;
        std::cout << "or " + string(argv[0]) +" -p proto -s source -o passed" << std::endl;
        curl_global_cleanup();
        exit(0);
    }
    vector<string> proxy_passed;
    vector<std::future<string>> checker_schedule;

    std::ifstream proxy_file;
    std::ofstream output_file;

    if(argv[1]!="-p") {
        // Open target files
        proxy_file = std::ifstream(argv[1]);
        output_file = std::ofstream(argv[2]);
    } else {
        proxy_file = std::ifstream(argv[4]);
        output_file = std::ofstream(argv[6]);
    }
    string proxy_buffer;
    if(argv[1]!="-p") {
        while (getline(proxy_file, proxy_buffer)) {
            checker_schedule.push_back(std::async(proxychecker, &proxy_passed, proxy_buffer));
        }
    } else {
        while (getline(proxy_file, proxy_buffer)) {
            checker_schedule.push_back(std::async(proxychecker, &proxy_passed, string(argv[2])+string("://")+proxy_buffer));
        }
    }
    for(auto & i : checker_schedule){
        i.wait();
    }
    for(const auto & i : proxy_passed){
        output_file << i <<std::endl;
    }
    curl_global_cleanup();
}